Pod::Spec.new do |s|
  s.name     = 'ShareSDK'
  s.version  = '2.11.0'
  s.summary  = 'ShareSDK is a SNS share kit.'
  s.homepage = 'https://github.com/jcccn/ShareSDK-iOS'
  s.author   = { 'Chuncheng Jiang' => 'jccuestc@gmail.com' }
  s.license  = { :type => 'Copyright', :text => <<-LICENSE
                   Copyright © 2012-2014 mob All Rights Reserved 掌淘网络 版权所有
                 LICENSE
               }
  s.platform     = :ios, '5.1.1'
  s.requires_arc = true

  s.default_subspec = 'Frequent'

  ### Subspecs

  s.subspec 'Frequent' do |fs|
    fs.requires_arc = true
    fs.dependency 'ShareSDK/SinaWeibo'
    fs.dependency 'ShareSDK/WeChat'
    fs.dependency 'ShareSDK/QQConnect'
    fs.dependency 'ShareSDK/QZone'
  end

  s.subspec 'Core' do |cs|
    cs.requires_arc = true
    cs.frameworks = 'SystemConfiguration', 'QuartzCore', 'CoreTelephony'
    cs.libraries  = 'icucore', 'z.1.2.5'
    cs.vendored_frameworks = 'ShareSDK/ShareSDK.framework', 'ShareSDK/Core/AGCommon.framework', 'ShareSDK/Core/ShareSDKCoreService.framework'
    cs.resources = ["ShareSDK/Core/Resource.bundle", "ShareSDK/Core/zh-Hans.lproj/*.strings"]
  end

  s.subspec 'SinaWeibo' do |sinaweibos|
    sinaweibos.requires_arc = true
    sinaweibos.dependency 'ShareSDK/Core'
    sinaweibos.frameworks = 'ImageIO'
    sinaweibos.source_files   = 'ShareSDK/Extend/SinaWeiboSDK/WeiboSDK.h'
    sinaweibos.vendored_frameworks = 'ShareSDK/Connection/SinaWeiboConnection.framework'
    sinaweibos.vendored_libraries = 'ShareSDK/Extend/SinaWeiboSDK/libSinaWeiboSDK.a'
    sinaweibos.resources = "ShareSDK/Extend/SinaWeiboSDK/WeiboSDK.bundle"
  end

  s.subspec 'WeChat' do |wechats|
    wechats.requires_arc = true
    wechats.dependency 'ShareSDK/Core'
    wechats.source_files   = 'ShareSDK/Extend/WeChatSDK/WXApi.h', 'ShareSDK/Extend/WeChatSDK/WXApiObject.h'
    wechats.vendored_frameworks = 'ShareSDK/Connection/WeChatConnection.framework'
    wechats.vendored_libraries = 'ShareSDK/Extend/WeChatSDK/libWeChatSDK.a'
  end

  s.subspec 'QZone' do |qzones|
    qzones.requires_arc = true
    qzones.dependency 'ShareSDK/QQConnect'
    qzones.vendored_frameworks = 'ShareSDK/Connection/QZoneConnection.framework'
  end

  s.subspec 'QQConnect' do |qqconnects|
    qqconnects.requires_arc = true
    qqconnects.dependency 'ShareSDK/Core'
    qqconnects.frameworks = 'Security'
    qqconnects.libraries  = 'stdc++', 'sqlite3'
    qqconnects.vendored_frameworks = 'ShareSDK/Connection/QQConnection.framework', 'ShareSDK/Extend/QQConnectSDK/TencentOpenAPI.framework'
    qqconnects.resources = "ShareSDK/Extend/QQConnectSDK/TencentOpenApi_IOS_Bundle.bundle"
  end
end
